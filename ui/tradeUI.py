"""
Very basic and incomplete main test class for Jython Swing UI for Trade Dangerous.

WARNING!!! very initial work in progress and definitely not ready for public viewing/comment let alone use! Just
checking in so I don't lose things as I switch between working on this on my laptop and home PC in spare moments of
time.

To repeat, not for use and does nothing at the moment, still wrestling with LayoutManagers, and mockup UI is very
incomplete and not yet connected to dummy data let alone TD.

Requires JDK 1.8.0_25 or JRE of same, Jython 2.5.3 and JAVA_HOME and JYTHON_HOME to be set in users path, and
appropriate libs in users CLASSPATH. These env variables could be set globally in the users .bashrc, or in a shell
script or batch file which would run this UI. The appropriate variables should appear at the very start of both PATH
and CLASSPATH, otherwise they may not be picked up if the user has another Java (or Jython) version installed, which is
often the case.

Example of a .bashrc to run this ui with Trade Dangerous:

    HISTSIZE=5000
    HISTFILESIZE=10000
    shopt -s histappend

    ### CHANGE THIS TO YOUR TradeDangerous path
    ### (Just don't include the 'trade.py' part)
    export TRADEDIR="/d/src/mh-tradedangerous"

    # Add the scripts folder and Python and Java to the bash path. Change these to your installation locations!
    # note that JDK_VERSION can also be a JRE installation path if desired. Having both JDK_VERSION and
    # JAVA_HOME variables allows for easy swapping of different version JDKs/JREs if necessary if they are all
    # installed under the JAVA_HOME dir
    PYTHON_HOME="/c/Python34/"
    JDK_VERSION="jdk1.8.0_25"
    JAVA_HOME="/d/apps/Java/${JDK_VERSION}"
    JAVA_PATH="${JAVA_HOME}/bin"
    JAVA_LIB="${JAVA_HOME}/lib"
    JYTHON_HOME="/d/apps/jython2.5.3"

    # export PATH, CLASSPATH and ensure TD scripts dir is also in PATH
    export PATH="${PYTHON_HOME}:${JYTHON_HOME}/bin:${JAVA_PATH}:${TRADEDIR}/scripts:${PATH}"
    export CLASSPATH="${JAVA_LIB}:${JYTHON_HOME}/lib:${CLASSPATH}"

    cd ${TRADEDIR}
    \

Once the above is all setup, and assuming you have bash, you can run this UI from ${TRADEDIR} at the command line by typing:
    jython ui/tradeUI.py

For now users will have to install JDK and Jython themselves, in future, who knows?
At some point when things are more concrete and complete, I will create a .sh file and a .bat file for this stuff.
TODO switch to Jython 2.7 when it is out of final beta

Copyright (c) Martin Hubley $today.year. under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
See full license at: http://creativecommons.org/licenses/by-nc-sa/4.0/
"""


from javax.swing import *
from javax.swing import JTable
from javax.swing.tree import *
from java.lang import *
from java.util import *
from java.awt import *
from jarray import *

class tradeUI:

    def __init__(self):

        debug = True

        self.frame = JFrame("Trade Dangerous Jython Swing UI Mockup")

        # create main window and add our main JTabPane to the main window, set size, visible etc.

        # grab OS screen size from system as Dimension
        screenSize = Toolkit.getDefaultToolkit().getScreenSize()
        if debug:
            print str(screenSize)
        self.frame.setSize(screenSize) # use setSize for top level window w/o LayoutManager
        self.frame.setPreferredSize(screenSize)
        self.frame.setMinimumSize(Dimension(1000, 800)) # saves a whole lot of messing about with LayoutManagers for silly small sizes
        self.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
        self.frame.setLocationRelativeTo(None)

        self.tabPane = JTabbedPane(JTabbedPane.TOP)

        self.systemsPanel = self.setupSystemsPanel()
        self.tdRunPanel = self.setupRunPanel()
        self.tdNavPanel = self.setupNavPanel()
        self.tdLocalPanel = self.setupLocalPanel()
        # TODO use cases/panel setup for any other tabs?

        # add our four main panels to the appropriate JTabPane
        self.tabPane = JTabbedPane()
        self.tabPane.add("Systems, Stations and Commodities", self.systemsPanel)
        self.tabPane.add("Trade Run", self.tdRunPanel)
        self.tabPane.add("Navigation", self.tdNavPanel)
        self.tabPane.add("Local Systems", self.tdLocalPanel)
        self.tabPane.setPreferredSize(self.frame.getPreferredSize())

        # add the tab pane and show the frame
        self.frame.add(self.tabPane)
        self.frame.pack()
        self.frame.setVisible(True)

    def setupLocalPanel(self):
        # the main panel for doing trade.py local
        return JPanel()
        # TODO


    def setupNavPanel(self):

        # the main panel for doing trade.py nav
        return JPanel()
        # TODO

    def setupRunPanel(self):
        # The main panel for conducting trade.py run
        # TODO proper layout and logic for loading initial data (or load in listeners?) and out in another method for each tab setup
        # TODO set all defaults from config.sh, validation, layout
        tdRunPanel = JPanel()

        argsPanel = JPanel()

        # mandatory run command arguments
        cmbTo = JComboBox()
        lblTo = JLabel("To: ")
        argsPanel.add(lblTo)
        argsPanel.add(cmbTo)
        # TODO populate

        cmbFrom = JComboBox()
        lblFrom = JLabel("From: ")
        argsPanel.add(lblFrom)
        argsPanel.add(cmbFrom)
        # TODO populate

        txtJumps = JTextField(4)
        lblJumps = JLabel("Jumps: ")
        argsPanel.add(lblJumps)
        argsPanel.add(txtJumps)
        # TODO set default from config.sh

        argsConstraints = GridBagConstraints()
        argsConstraints.fill = GridBagConstraints.HORIZONTAL # natural height, maximum width
        argsConstraints.anchor = GridBagConstraints.FIRST_LINE_START
        argsConstraints.gridx = 0
        argsConstraints.gridy = 0

        tdRunPanel.add(argsPanel, argsConstraints)

        #subpanel for mandatory ship/player information
        shipInfoPanel = JPanel()

        txtCredits = JTextField(15)
        lblCredits = JLabel("Credits: ")
        shipInfoPanel.add(lblCredits)
        shipInfoPanel.add(txtCredits)

        txtCapacity = JTextField(4)
        lblCapacity = JLabel("Capacity: ")
        shipInfoPanel.add(lblCapacity)
        shipInfoPanel.add(txtCapacity)

        txtEmptyLy = JTextField(4)
        lblEmptyLy = JLabel("Empty Ly: ")
        shipInfoPanel.add(lblEmptyLy)
        shipInfoPanel.add(txtEmptyLy)

        txtLyPer = JTextField(4)
        lblLyPer = JLabel("Ly Per: " )
        shipInfoPanel.add(lblLyPer)
        shipInfoPanel.add(txtLyPer)

        shipInfoConstraints = GridBagConstraints()
        shipInfoConstraints.fill = GridBagConstraints.HORIZONTAL # natural height, maximum width
        shipInfoConstraints.anchor = GridBagConstraints.PAGE_START
        shipInfoConstraints.gridx = 2
        shipInfoConstraints.gridy = 0

        tdRunPanel.add(shipInfoPanel, shipInfoConstraints)


        # optional run command arguments TODO could have optional panel and disable until selected?
        optArgsPanel = JPanel()

        txtHops = JTextField(4)
        lblHops = JLabel("Hops: ")
        optArgsPanel.add(lblHops)
        optArgsPanel.add(txtHops)

        txtStartJumps = JTextField(4)
        lblStartJumps = JLabel("Start Jumps: ")
        optArgsPanel.add(lblStartJumps)
        optArgsPanel.add(txtStartJumps)

        txtEndJumps = JTextField(4)
        lblEndJumps = JLabel("End Jumps: ")
        optArgsPanel.add(lblEndJumps)
        optArgsPanel.add(txtEndJumps)

        cmbVia = JComboBox()
        lblVia = JLabel("Via: ")
        optArgsPanel.add(lblVia)
        optArgsPanel.add(cmbVia)

        cmbAvoid = JComboBox()
        lblAvoid = JLabel("Avoid: ")
        optArgsPanel.add(lblAvoid)
        optArgsPanel.add(cmbAvoid)

        txtInsurance = JTextField(15)
        lblInsurance = JLabel("Insurance: ")
        optArgsPanel.add(lblInsurance)
        optArgsPanel.add(txtInsurance)

        optArgsConstraints = GridBagConstraints()
        optArgsConstraints.fill = GridBagConstraints.HORIZONTAL # natural height, maximum width
        optArgsConstraints.anchor = GridBagConstraints.FIRST_LINE_END
        optArgsConstraints.gridx = 3
        optArgsConstraints.gridy = 0

        tdRunPanel.add(optArgsPanel, optArgsConstraints)

        # second row of tdRunPanel

        # subpanel for commodity information
        commodityInfoPanel = JPanel()
        commConstraints = GridBagConstraints()
        commConstraints.fill = GridBagConstraints.HORIZONTAL # natural height, maximum width
        commConstraints.anchor = GridBagConstraints.LINE_START
        commConstraints.gridx = 0
        commConstraints.gridy = 1

        lblLimit = JLabel("Limit: ")
        txtLimit = JTextField(4)
        commodityInfoPanel.add(lblLimit)
        commodityInfoPanel.add(txtLimit)

        lblMargin = JLabel("Margin: ")
        txtMargin = JTextField(4)
        commodityInfoPanel.add(lblMargin)
        commodityInfoPanel.add(txtMargin)

        cmbAvoidComm = JComboBox()
        lblAvoidComm = JLabel("Avoid Commodity: ")
        commodityInfoPanel.add(lblAvoidComm)
        commodityInfoPanel.add(cmbAvoidComm)

        lblMaxDaysOld = JLabel("Max Days Old: ")
        txtMaxDaysOld = JTextField(4)
        commodityInfoPanel.add(lblMaxDaysOld)
        commodityInfoPanel.add(txtMaxDaysOld)

        tdRunPanel.add(commodityInfoPanel, commConstraints)

        # subpanel for optional booleans, all on by default for now TODO load/remember user preferences not in config.sh
        boolPanel = JPanel()
        boolConstraints = GridBagConstraints()
        boolConstraints.fill = GridBagConstraints.HORIZONTAL # natural height, maximum width
        boolConstraints.anchor = GridBagConstraints.CENTER
        boolConstraints.gridx = 1
        boolConstraints.gridy = 1

        chkUnique = JCheckBox("Unique", False)
        boolPanel.add(chkUnique)

        chkBlackMarket = JCheckBox("Black Market", False)
        boolPanel.add(chkBlackMarket)

        lblPadSize = JLabel("Pad Size: ")
        chkSmall = JCheckBox ("Small", True)
        boolPanel.add(lblPadSize)
        boolPanel.add(chkSmall)

        chkMedium = JCheckBox ("Medium", True)
        boolPanel.add(chkMedium)

        chkLarge = JCheckBox("Large", True)
        boolPanel.add(chkLarge)

        tdRunPanel.add(boolPanel, boolConstraints)

        # run results JTextArea, allows C^P
        taResults = JTextArea()
        resultsConstraints = GridBagConstraints()
        resultsConstraints.fill = GridBagConstraints.BOTH # maximize height and width
        resultsConstraints.anchor = GridBagConstraints.LAST_LINE_START
        resultsConstraints.gridx = 0
        resultsConstraints.gridy = 2

        tdRunPanel.add(taResults, resultsConstraints)

        return tdRunPanel

    def setupSystemsPanel(self):

        # TODO dummy content for now, replace with JTree etc

        # System JTree
        systemPanel = JPanel()
        topNode = DefaultMutableTreeNode("Systems")

        # run through the nodes and add station data - otherwise could use array with systems and stations
        for x in range (0, 9):
            system = DefaultMutableTreeNode("System " + str(x))
            topNode.add(system)
            for x in range (0, 3):
                station = DefaultMutableTreeNode("Station " + str(x))
                system.add(station)

        systemTree = JTree(topNode)

        # Commodities Panel

        # columnNames = array(["Commodity", "Sell", "Buy", "Demand", "Stock", "Galactic Hi", "Galactic Lo", "Galactic Average"], Object)
        columnNames = Vector()
        columnNames.addElement("Commodity")
        columnNames.addElement("Sell")
        columnNames.addElement("Buy")
        columnNames.addElement("Demand")
        columnNames.addElement("Stock")
        columnNames.addElement("Galactic Hi")
        columnNames.addElement("Galactic Lo")
        columnNames.addElement("Galactic Avg")

        # TODO need to display all commodities by category as well as rares
        # Could use tree view for categories, and then have a table as each node of the tree?

        # grain = array(["Grain","210","190" "?", "5346L", "219", "99", "147"], Object)
        # animal = array(["Animal Meat","1210","1190" "?", "15346M", "2190", "990", "1470"], Object)
        # gold = array(["Gold","4100","-" "123H", "-", "5219", "3999", "4147"], Object)
        # bauxite = array(["Bauxite","54","60" "?", "1345346M", "32", "70", "43"], Object)

        commodityData = Vector()
        for x in range (1, 9):
            rowData = Vector()
            rowData.addElement("Commodity " + str(x))
            rowData.addElement(x * 10 )
            rowData.addElement(x * 20 )
            rowData.addElement("-")
            rowData.addElement(str(x * 100) + "L" )
            rowData.addElement(x * 22 )
            rowData.addElement(x * 8)
            rowData.addElement(x * 12)
            commodityData.addElement(rowData)

        commoditiesTable = JTable(commodityData, columnNames)
        commoditiesPane = JScrollPane(commoditiesTable)
        commoditiesTable.setFillsViewportHeight(True);

        for i in range (0,8):
            column = commoditiesTable.getColumnModel().getColumn(i)
            # column.setPreferredWidth(100);


        # Station JPanel
        stationPanel = JPanel()

        self.txtSystem = JTextField('System name',15)
        stationPanel.add(self.txtSystem)

        self.txtStation = JTextField('Station name', 15)
        stationPanel.add(self.txtStation)

        self.txtDistLs = JTextField('Dist ls', 15)
        stationPanel.add(self.txtDistLs)

        self.txtBlackMarket = JTextField('BMt', 3)
        stationPanel.add(self.txtBlackMarket)

        self.txtMaxPadSize = JTextField('Pad', 3)
        stationPanel.add(self.txtMaxPadSize)

        btnSave = JButton('Add/Update Station')
        #btnSave = JButton('Add/Update Station',actionPerformed=self.addStation)
        stationPanel.add(btnSave)

        # layout the main panel
        systemPanel.setLayout(GridBagLayout())
        systemPanel.setPreferredSize(self.frame.getPreferredSize()) # in theory this should be the maximum size of any tab on the tab pane

        treeConstraints = GridBagConstraints()
        # treeConstraints.fill = GridBagConstraints.VERTICAL # natural width, maximum height
        treeConstraints.fill = GridBagConstraints.BOTH
        treeConstraints.anchor = GridBagConstraints.LINE_START
        treeConstraints.gridx = 0
        treeConstraints.gridy = 0
        treeConstraints.gridheight = 2 # use default value of 1 for gridwidth, maximize grid height

        stationConstraints = GridBagConstraints()
        stationConstraints.fill = GridBagConstraints.HORIZONTAL # natural height, maximum width
        stationConstraints.anchor = GridBagConstraints.PAGE_START
        stationConstraints.gridx = 1
        stationConstraints.gridy = 0

        commoditiesConstraints = GridBagConstraints()
        # commoditiesConstraints.fill = GridBagConstraints.HORIZONTAL # natural height, maximum width
        commoditiesConstraints.fill = GridBagConstraints.BOTH
        commoditiesConstraints.anchor = GridBagConstraints.CENTER
        commoditiesConstraints.gridx = 1
        commoditiesConstraints.gridy = 2


        systemPanel.add(systemTree, treeConstraints)
        systemPanel.add(stationPanel, stationConstraints)
        systemPanel.add(commoditiesPane, commoditiesConstraints)

        return systemPanel

if __name__ == '__main__':
    #start things off
    tradeUI()

"""

  TODO this needs to be moved into a separate module/class file. This is just a simple class for testing for now

"""

class tdLogic:

    strSystem = ""

    strStation = ""

    strDistLs = "?"

    strBlackMarket = "?"

    strMaxPadSize = "?"

    def __addStation__(self):
        print "In addStation"
        # normally we would have validation and all that good stuff here but for now I just want to get it working
        # as a test
        self.strSystem = self.txtSystem.getText()
        self.strStation  = self.txtStation.getText()
        self.strDistLs = self.txtDistLs.getText()
        self.strBlackMarket = self.txtBlackMarket.getText()
        self.strMaxPadSize = self.txtMaxPadSize.getText()


    def __updateStation__(self):
        print "In updateStation"

    def __deleteStation__(self):
        print "In deleteStation"

    def __init__(self):
        print "In tdLogic init()"










